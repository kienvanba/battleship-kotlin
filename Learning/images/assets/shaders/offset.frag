#ifdef GL_ES                
precision mediump float;   
#endif

varying vec4 v_color;      
varying vec2 v_texCoords;  

uniform sampler2D u_texture;
uniform float time;
uniform float timedelta;
uniform float amplituda;
uniform float height;

void main()                
{           
	float t = v_texCoords.y + (sin(v_texCoords.x * amplituda + timedelta) * height);
	vec2 distort = vec2(v_texCoords.x - time, t);
	gl_FragColor = v_color * texture2D(u_texture, distort);  
}          