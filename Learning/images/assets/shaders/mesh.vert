attribute vec4 a_position; 
attribute vec4 a_color;    
attribute vec2 a_texCoords;  

uniform mat4 u_projTrans;	

varying vec4 vColor;
varying vec2 vTexCoord;

void main()                
{                          
	vColor = a_color;        
	vTexCoord = a_texCoords; 
	gl_Position =  u_projTrans * vec4(a_position.xy, a_position.z, 1.0);
}