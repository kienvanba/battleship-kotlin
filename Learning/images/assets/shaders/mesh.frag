#ifdef GL_ES                
precision mediump float;   
#endif

varying vec4 vColor;      
varying vec2 vTexCoord;  

uniform sampler2D u_texture;
uniform float point;
uniform float dX;


void main()                
{           
	vec4 texColor = texture2D(u_texture, vTexCoord);
	gl_FragColor = vec4(vec3((point + dX * vTexCoord.s) * texColor), texColor.a);
}          