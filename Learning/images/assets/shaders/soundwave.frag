#ifdef GL_ES
    precision mediump float;
#endif

varying vec4 vColor;
varying vec2 vTexCoord;

uniform sampler2D u_texture;
uniform sampler2D u_displ;
uniform mat4 u_projTrans;

uniform vec2 resolution;
uniform vec2 res_max;

uniform vec2 sizeDispl;
uniform vec2 posDispl;

uniform vec2 sizeTex;
uniform vec2 posTex;


void main()
{
	//float dS = (gl_FragCoord.x - posDispl.x * (resolution.x/res_max.x))/sizeDispl.x;
	//float dT = (gl_FragCoord.y - posDispl.y * (resolution.y/res_max.y))/sizeDispl.y;
	
	float dS = (gl_FragCoord.x / (resolution.x/res_max.x) - posDispl.x)/sizeDispl.x;
	float dT = (gl_FragCoord.y / (resolution.y/res_max.y) - posDispl.y)/sizeDispl.y;
	
	vec2 texCoordD = vec2(dS, dT);
	vec2 displacement = texture2D(u_displ, texCoordD).xy;
	
	float s = vTexCoord.x + displacement.x * 0.009;
	float t = vTexCoord.y + displacement.y * 0.009;
	
	gl_FragColor = vColor * texture2D(u_texture, vec2(s, t));
}