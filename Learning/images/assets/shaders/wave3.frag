#ifdef GL_ES
    precision mediump float;
#endif

varying vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform sampler2D u_texture2;
uniform float timedelta;
uniform float amplituda;
uniform float height;

void main()
{
	vec2 displacement = texture2D(u_texture2, v_texCoords).xy;
	float t = v_texCoords.y + displacement.y * 0.17 - 0.05 + (sin(v_texCoords.x * amplituda + timedelta) * height);
	gl_FragColor = v_color * texture2D(u_texture, vec2(v_texCoords.x, t));
}