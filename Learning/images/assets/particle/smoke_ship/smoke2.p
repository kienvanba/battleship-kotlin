Untitled
- Delay -
active: false
- Duration - 
lowMin: 3500.0
lowMax: 3500.0
- Count - 
min: 20
max: 200
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 30.0
highMax: 30.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 2500.0
highMax: 3500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Offset - 
active: true
lowMin: -5.0
lowMax: 5.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 10.0
lowMax: 20.0
highMin: 5.0
highMax: 10.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Velocity - 
active: true
lowMin: 5.0
lowMax: 15.0
highMin: 30.0
highMax: 35.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Angle - 
active: true
lowMin: 10.0
lowMax: 1.0
highMin: 60.0
highMax: 90.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.74509805
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.70547944
timeline2: 1.0
- Rotation - 
active: true
lowMin: 1.0
lowMax: -360.0
highMin: 1.0
highMax: 360.0
relative: true
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 10.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 0.019607844
scaling2: 1.0
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.30136988
timeline2: 0.6712329
timeline3: 1.0
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 1.0
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.31578946
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.10273973
timeline2: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: false
behind: false
- Image Path -
D:\Work\SeaBattle2\Scenes\Particles\Game_select_scene\water_p1.png


Untitled
- Delay -
active: false
- Duration - 
lowMin: 3000.0
lowMax: 3000.0
- Count - 
min: 5
max: 10
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 10.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1500.0
highMax: 3000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 2.0
lowMax: 4.0
highMin: 5.0
highMax: 7.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 10.0
highMin: 15.0
highMax: 20.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Angle - 
active: true
lowMin: 100.0
lowMax: 100.0
highMin: 30.0
highMax: 90.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.7254902
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.69863015
timeline2: 1.0
- Rotation - 
active: true
lowMin: 1.0
lowMax: -360.0
highMin: 1.0
highMax: 360.0
relative: true
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 1.0
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.3859649
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.11643836
timeline2: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: false
behind: false
- Image Path -
D:\Work\SeaBattle2\Scenes\Particles\Game Scene\spark.png
