Untitled
- Delay -
active: false
- Duration - 
lowMin: 1500.0
lowMax: 1500.0
- Count - 
min: 10
max: 35
- Emission - 
lowMin: 5.0
lowMax: 10.0
highMin: 25.0
highMax: 35.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 300.0
lowMax: 300.0
highMin: 500.0
highMax: 1500.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life Offset - 
active: false
- X Offset - 
active: true
lowMin: -3.0
lowMax: 3.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 3.0
lowMax: 5.0
highMin: 20.0
highMax: 30.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Velocity - 
active: false
- Angle - 
active: true
lowMin: 90.0
lowMax: 90.0
highMin: 45.0
highMax: 135.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: true
lowMin: 1.0
lowMax: 1.0
highMin: 180.0
highMax: 180.0
relative: true
scalingCount: 2
scaling0: 1.0
scaling1: 0.49019608
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 1.0
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.7894737
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.20547946
timeline2: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: false
behind: false
- Image Path -
D:\Work\SeaBattle2\Scenes\Particles\Smoke.png
