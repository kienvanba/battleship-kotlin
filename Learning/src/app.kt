/**
 * Created by KIEN on 5/24/2017.
 */
fun main(args: Array<String>) {
    println("hello world")
    print("im kien"+"\n")
    println("im from room "+(10+1))
    val n:Int = 10
    /**
     * val is read only
     */
    println("n= "+(n+7))
    var myName:String=""" My name is
                        Kien"""
    println(myName)
    var arrX:IntArray= intArrayOf(1,2,3,5)
    println(arrX[1])
    /*
    var arrY:DoubleArray= doubleArrayOf(1.5,2.6,9.0,10.3)
    println(arrY[3])
    var arrC:CharArray= charArrayOf('a','b','c')
    println(arrC[0])
     */

    var double:Double = 10.99
    var int:Int = 10
    var intToDouble = int.toDouble()
    println("double = "+double)
    println("int to double = "+intToDouble)
    println("double to int = "+double.toInt())


//    print("what is your name: ")
//    val yourName = readLine()
//    println("well hello there "+(yourName!!.toInt()+1))
//    println("はじめまして")

    val max:Int = 10
    val min:Int = 1
    println(if(min>max)"true" else "false")
    val result:Boolean = if(min<max){print("min > max is ");false} else {print("min > max is ");true}
    println(result)

    /**
     * when = switch
     */
//    print("type some number from 0 to 9: ")
//    var line = readLine()
//    var number:Int = 0
//    if(line!=null)
//        number = line.toInt()
//    when(number){
//        0,2,4,6,8 -> println("even")
//        1,3,5,7,9 -> println("odd")
//        in 10..19 -> println("10 - 19")
//        in 20..29 -> println("20 - 29")
//        else -> println("did i tell you to type $number?")
//    }
//    val extra = when(number){
//        in 0..10 -> 100
//        in 20..30 -> 200
//        else -> number
//    }
//    when{
//        number%2==0 -> println("even")
//        else -> println("odd")
//    }
    /**
     * for
     * for(i in a..b) i=a -> 1++ -> 1=b
     * for(i in a until b) i=a -> i++ -> i=b-1
     * for(i in a..b step x) i=a -> i+=x -> i=b
     * downTo
     * downTo step
     */


    var mDate:DatBusCu = DatBusCu(1)
    println(mDate.toString())
    //sử dụng hàm nà.. đụ móa
    mDate.nextDate()
    println(mDate.toString())
}

/**
 * thêm hàm vào class nè
 */
fun DatBusCu.nextDate(){
    this.x+=1
}
/**
 * tạo class nè
 */
class DatBusCu(a:Int){
    var x:Int =a
    override fun toString(): String {
        return x.toString()
    }
}