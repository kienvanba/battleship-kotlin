package battleship

import battleship.scenes.WelcomeScene
import javax.swing.JFrame

/**
 * Created by KIEN on 6/8/2017.
 */
class Main: JFrame(){
    companion object {
        val WIDTH: Int = 407
        val HEIGHT: Int = 620
    }
    var mainScene:MainScene = MainScene()
    init {
        setSize(WIDTH, HEIGHT)
        isVisible = true
        defaultCloseOperation = EXIT_ON_CLOSE
        contentPane = mainScene
        isResizable = false
        validate()
        GameManager.addScenes(WelcomeScene())
        GameThread(mainScene).start()
    }
}

class GameThread(ms: MainScene): Thread(){
    val mainScene: MainScene = ms
    override fun run() {
        super.run()
        var lastTime:Long = System.nanoTime()
        val fps:Double = 60.0
        val ns = 1000000000/fps
        var delta:Double = 0.0
        while(GameManager.isRunning){
            val now:Long = System.nanoTime()
            delta +=(now-lastTime)/ns
            lastTime = now
            if(delta>=1){
                mainScene.update()
                mainScene.repaint()
                delta--
            }
        }
    }
}

fun main(args: Array<String>) {
    val main:Main = Main()
}