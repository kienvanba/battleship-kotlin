package battleship

import battleship.scenes.IScene
import battleship.scenes.WelcomeScene
import java.awt.Font
import java.awt.GraphicsEnvironment
import java.io.File
import java.lang.Exception
import java.util.*

/**
 * Created by KIEN on 6/8/2017.
 */
object GameManager{
    var isRunning:Boolean = true
    var scenes: Stack<IScene> = Stack()
    var point:Int=0

    fun addScenes(scene: IScene){
        if(!scenes.isEmpty()){
            scenes.pop().dispose()
        }
        scenes.push(scene)
    }

    fun getCurrentScene():IScene{
        if(scenes.isEmpty())
            return WelcomeScene()
        return scenes.peek()
    }

    fun getFont(fontName: String): Font? {
        var font:Font? = null
        try {
            val file = File(fontName)
            font = Font.createFont(Font.TRUETYPE_FONT, file)
            val ge = GraphicsEnvironment.getLocalGraphicsEnvironment()
            ge.registerFont(font)
            font.size
            return font
        }catch (ex: Exception){
            println(ex.toString())
        }
        return font
    }
}