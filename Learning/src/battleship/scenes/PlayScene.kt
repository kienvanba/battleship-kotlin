package battleship.scenes

import battleship.GameManager
import battleship.Main
import battleship.models.*
import java.awt.Color
import java.awt.Font
import java.awt.Graphics
import java.awt.Point
import java.awt.event.KeyEvent
import java.awt.event.MouseEvent
import java.awt.image.BufferedImage
import java.io.File
import java.util.*
import javax.imageio.ImageIO

/**
 * Created by KIEN on 6/9/2017.
 */
class PlayScene : IScene{
    var ship: Ship = Ship(200.0,450.0)
    var bullets: ArrayList<Allies> = ArrayList()
    var enemies: ArrayList<Enemies> = ArrayList()
    var lastBombTime = System.currentTimeMillis()
    var lastPirateTime = System.currentTimeMillis()
    val heathBar:HeathBar = HeathBar(5.0,5.0,ship)
    val backgroundTexture:BufferedImage = ImageIO.read(File("src/battleship/assets/game-background.jpg"))
    val blurBackgroundTexture:BufferedImage = ImageIO.read(File("src/battleship/assets/blur-background.png"))
    val dropMenuTexture:BufferedImage = ImageIO.read(File("src/battleship/assets/drop-menu-btn.png"))
    val dropMenuTextureHover:BufferedImage = ImageIO.read(File("src/battleship/assets/drop-menu-btn-hover.png"))
    val btnResumeTexture:BufferedImage = ImageIO.read(File("src/battleship/assets/resume-btn.png"))
    val btnResumeTextureHover:BufferedImage = ImageIO.read(File("src/battleship/assets/resume-btn-hover.png"))
    val btnDrop: Button = Button(Main.WIDTH-60.0, 10.0, dropMenuTexture, dropMenuTextureHover)
    val btnResume:Button = Button(Main.WIDTH/2-40.0, -100.0, btnResumeTexture, btnResumeTextureHover)
    val dropMenu:IngameDropMenu = IngameDropMenu()
    val font = GameManager.getFont("src/battleship/assets/seabattle.ttf")
    init {
        GameManager.point = 0
        btnResume.speed = 30
        dropMenu.speed = 10
    }
    var isPause:Boolean = false
    var switchSceneTiming = 0

    override fun render(g: Graphics) {
        g.drawImage(backgroundTexture, 0, 0, Main.WIDTH, Main.HEIGHT, null)
        ship.render(g)
        (0 until bullets.size)
                .filter { it <bullets.size }
                .forEach { bullets[it].render(g) }
        for(enemy in enemies){
            enemy.render(g)
        }
        heathBar.render(g)
        if(font!=null){
            val mFont = Font(font.fontName, Font.BOLD, 50)
            g.font = mFont
        }
        g.color = Color.blue
        g.drawString(GameManager.point.toString(), 10, 100)
        if(isPause) {
            g.drawImage(blurBackgroundTexture, 0, 0, Main.WIDTH, Main.HEIGHT, null)
            btnResume.render(g)
        }
        btnDrop.render(g)
        dropMenu.render(g)
    }

    override fun update() {
        if(!isPause) {
            ship.update()
            spawnEnemies()
            controlGameObject()
            heathBar.update()
            btnDrop.update()
        }
        btnResume.update()
        dropMenu.update()
        if(ship.isBroken){
            switchSceneTiming++
            if(switchSceneTiming==100)
                GameManager.addScenes(EndScene())
        }
    }

    override fun onKeyPressed(e: KeyEvent) {
        if(isPause)return
        when(e.keyCode){
            KeyEvent.VK_LEFT, KeyEvent.VK_A -> ship.velocityX = -Ship.SPEED
            KeyEvent.VK_RIGHT, KeyEvent.VK_D -> ship.velocityX = Ship.SPEED
            KeyEvent.VK_UP, KeyEvent.VK_W -> ship.velocityY = -Ship.SPEED
            KeyEvent.VK_DOWN, KeyEvent.VK_S -> ship.velocityY = Ship.SPEED
        }
    }

    override fun onKeyReleased(e: KeyEvent) {
        if(!isPause) {
            when (e.keyCode) {
                KeyEvent.VK_LEFT, KeyEvent.VK_A -> ship.velocityX = 0.0
                KeyEvent.VK_RIGHT, KeyEvent.VK_D -> ship.velocityX = 0.0
                KeyEvent.VK_UP, KeyEvent.VK_W -> ship.velocityY = 0.0
                KeyEvent.VK_DOWN, KeyEvent.VK_S -> ship.velocityY = 0.0
                KeyEvent.VK_ESCAPE -> {isPause = true; btnResume.destinationY=Main.HEIGHT/2-40.0;}
            }
        }else{
            if(e.keyCode == KeyEvent.VK_ESCAPE) {
                isPause = false
                btnResume.destinationY = btnResume.rootY
                dropMenu.destinationY = dropMenu.root
            }
        }
    }

    override fun onMousePressed(e: MouseEvent) {
        val clickPoint = Point(e.x, e.y)
        if(!isPause) {
            if (!ship.isBroken && !btnDrop.getBound().contains(clickPoint))
                bullets.add(Bullet(ship.positionX + 10, ship.positionY + 15))
        }else{
            btnResume.isHovering = btnResume.getBound().contains(clickPoint)
        }
        btnDrop.isHovering = btnDrop.getBound().contains(clickPoint)
        dropMenu.onMousePressed(e)
    }

    override fun onMouseReleased(e: MouseEvent) {
        val releasePoint:Point = Point(e.x, e.y)
        btnDrop.isHovering = false
        btnResume.isHovering = false
        if(btnDrop.getBound().contains(releasePoint) && dropMenu.positionY<0){
            dropMenu.destinationY = 10.0
            btnResume.destinationY = Main.HEIGHT/2-40.0
            isPause = true
        }
        if(btnResume.getBound().contains(releasePoint) && isPause){
            btnResume.destinationY = btnResume.rootY
            dropMenu.destinationY = dropMenu.root
            isPause = false
        }
        dropMenu.onMouseReleased(e)
    }

    override fun dispose() {
    }

    /**
        todo: perform action when collided, remove object when collided or get out of screen
     */
    fun controlGameObject(){
        ship.collideReact(enemies)
        for(i in 0 until bullets.size){
            if(i<bullets.size) {
                bullets[i].update()
                bullets[i].collideReact(enemies)
                if (bullets[i].positionY < 0 || bullets[i].isDestroyed)
                    bullets.removeAt(i)
            }
        }
        val iterator = enemies.iterator()
        for(it in iterator){
            it.update()
            if(it.positionY> Main.HEIGHT || it.isDestroyed)
                iterator.remove()
        }
    }

    /**
        todo: spawning enemies at random point
     */
    fun spawnEnemies(){
        if(ship.isBroken)return
        if(System.currentTimeMillis()-lastBombTime>=5000){
            enemies.add(Bomb((Random().nextInt(400)-30).toDouble(), -20.0))
            lastBombTime = System.currentTimeMillis()
        }
        if(System.currentTimeMillis()-lastPirateTime>=2000){
            enemies.add(Pirate((Random().nextInt(400)-30).toDouble(), -20.0))
            lastPirateTime = System.currentTimeMillis()
        }
    }
}