package battleship.scenes

import battleship.GameManager
import battleship.Main
import battleship.models.*
import java.awt.Graphics
import java.awt.Point
import java.awt.event.KeyEvent
import java.awt.event.MouseEvent
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

/**
 * Created by KIEN on 6/8/2017.
 */
class WelcomeScene: IScene{
    val backgroundTexture:BufferedImage = ImageIO.read(File("src/battleship/assets/welcome-background-2.jpg"))
    val btnPlayTexture: BufferedImage = ImageIO.read(File("src/battleship/assets/play-btn.png"))
    val btnPlayTextureHover: BufferedImage = ImageIO.read(File("src/battleship/assets/play-btn_hover.png"))
    val btnExitTextureHover: BufferedImage = ImageIO.read(File("src/battleship/assets/exit-btn-hover.png"))
    val btnExitTexture: BufferedImage = ImageIO.read(File("src/battleship/assets/exit-btn.png"))
    val btnSettingTexture: BufferedImage = ImageIO.read(File("src/battleship/assets/setting-btn.png"))
    val btnSettingTextureHover: BufferedImage = ImageIO.read(File("src/battleship/assets/setting-btn-hover.png"))
    val btnPlay: Button = Button(100.0, Main.HEIGHT.toDouble()+10, btnPlayTexture, btnPlayTextureHover)
    val btnExit: Button = Button(100.0, Main.HEIGHT.toDouble()+10, btnExitTexture, btnExitTextureHover)
    val btnSetting: Button = Button(Main.WIDTH.toDouble()+10, Main.HEIGHT.toDouble()+10, btnSettingTexture, btnSettingTextureHover)

    init {
        btnPlay.speed = 10
        btnPlay.destinationX = 100.0
        btnPlay.destinationY = 300.0
        btnExit.destinationX = 100.0
        btnExit.destinationY = 400.0
        btnSetting.destinationX = 300.0
        btnSetting.destinationY = 500.0
    }

    override fun render(g: Graphics) {
        g.drawImage(backgroundTexture, 0, 0, 400, 800, null)
        btnPlay.render(g)
        btnExit.render(g)
        btnSetting.render(g)
    }

    override fun update() {
        btnPlay.update()
        btnExit.update()
        btnSetting.update()
    }

    override fun onKeyPressed(e: KeyEvent) {
    }

    override fun onKeyReleased(e: KeyEvent) {
    }

    override fun onMousePressed(e: MouseEvent) {
        val clickPoint = Point(e.x, e.y)
        if(btnPlay.getBound().contains(clickPoint))btnPlay.isHovering=true
        if(btnExit.getBound().contains(clickPoint))btnExit.isHovering=true
        if(btnSetting.getBound().contains(clickPoint))btnSetting.isHovering=true
    }

    override fun onMouseReleased(e: MouseEvent) {
        btnPlay.isHovering = false
        btnExit.isHovering = false
        btnSetting.isHovering = false
        val releasePoint = Point(e.x, e.y)
        if(btnPlay.getBound().contains(releasePoint)){
            GameManager.addScenes(PlayScene())
        }
        if(btnExit.getBound().contains(releasePoint)){
            System.exit(0)
        }
    }

    override fun dispose() {
    }
}