package battleship.scenes

import java.awt.Graphics
import java.awt.event.KeyEvent
import java.awt.event.MouseEvent

/**
 * Created by KIEN on 6/8/2017.
 */
interface IScene{
    fun update()
    fun render(g: Graphics)
    fun onKeyPressed(e: KeyEvent)
    fun onKeyReleased(e: KeyEvent)
    fun onMousePressed(e: MouseEvent)
    fun onMouseReleased(e: MouseEvent)
    fun dispose()
}