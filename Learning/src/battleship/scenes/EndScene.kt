package battleship.scenes

import battleship.GameManager
import battleship.Main
import battleship.models.Allies
import battleship.models.Button
import battleship.models.PlaneFromLeft
import battleship.models.PlaneFromRight
import java.awt.Color
import java.awt.Font
import java.awt.Graphics
import java.awt.Point
import java.awt.event.KeyEvent
import java.awt.event.MouseEvent
import java.awt.image.BufferedImage
import java.io.File
import java.util.*
import javax.imageio.ImageIO
import kotlin.collections.ArrayList

/**
 * Created by KIEN on 6/18/2017.
 */
class EndScene: IScene{
    val backgroundTexture:BufferedImage = ImageIO.read(File("src/battleship/assets/game-background.jpg"))
    val scoreBackgroundTexture:BufferedImage = ImageIO.read(File("src/battleship/assets/end-game-background.png"))
    val blurBackgroundTexture:BufferedImage = ImageIO.read(File("src/battleship/assets/blur-background.png"))
    val btnResumeTexture:BufferedImage = ImageIO.read(File("src/battleship/assets/resume-btn.png"))
    val btnResumeTextureHover:BufferedImage = ImageIO.read(File("src/battleship/assets/resume-btn-hover.png"))
    val btnResume: Button = Button(50.0, 300.0, btnResumeTexture, btnResumeTextureHover)
    val font = GameManager.getFont("src/battleship/assets/seabattle.ttf")
    val rand:Random = Random()
    var leftPlaneTime = System.currentTimeMillis()
    var rightPlaneTime = leftPlaneTime
    val planes:ArrayList<Allies> = ArrayList()
    init {
        planes.add(PlaneFromLeft(rand.nextInt(300).toDouble()-300, -80.0))
    }

    override fun update() {
        btnResume.update()
        val iterator = planes.iterator()
        for(it in iterator){
            it.update()
            if(it.isDestroyed)
                iterator.remove()
        }
        spawnObjects()
    }

    override fun render(g: Graphics) {
        g.drawImage(backgroundTexture, 0, 0, Main.WIDTH, Main.HEIGHT, null)
        g.drawImage(blurBackgroundTexture, 0, 0, Main.WIDTH, Main.HEIGHT, null)
        for(plane in planes){
            plane.render(g)
        }
        g.drawImage(scoreBackgroundTexture, 0, 100, 400, 400, null)
        if(font!=null){
            val mFont = Font(font.fontName, Font.BOLD, 50)
            g.font = mFont
        }
        g.color = Color.blue
        g.drawString(GameManager.point.toString(), 250, 300)
        btnResume.render(g)
    }

    override fun onKeyPressed(e: KeyEvent) {
    }

    override fun onKeyReleased(e: KeyEvent) {
    }

    override fun onMousePressed(e: MouseEvent) {
        btnResume.isHovering = btnResume.getBound().contains(e.x, e.y)
    }

    override fun onMouseReleased(e: MouseEvent) {
        btnResume.isHovering = false
        if(btnResume.getBound().contains(e.x, e.y))
            GameManager.addScenes(WelcomeScene())
    }

    fun spawnObjects(){
        if(System.currentTimeMillis()-leftPlaneTime >= rand.nextInt(500)+2000) {
            planes.add(PlaneFromLeft(rand.nextInt(300).toDouble()-300, -80.0))
            leftPlaneTime = System.currentTimeMillis()
        }
        if(System.currentTimeMillis()-rightPlaneTime >= rand.nextInt(500)+3000) {
            planes.add(PlaneFromRight(rand.nextInt(300).toDouble()+300, -80.0))
            rightPlaneTime = System.currentTimeMillis()
        }
    }

    override fun dispose() {
    }
}