package battleship.models

import battleship.Main
import java.awt.Graphics
import java.awt.Rectangle
import java.awt.image.BufferedImage
import java.io.File
import java.util.*
import javax.imageio.ImageIO

/**
 * Created by KIEN on 6/18/2017.
 */
class PlaneFromRight(x:Double, y:Double): Allies(x,y){
    val planeFromRightTexture: BufferedImage = ImageIO.read(File("src/battleship/assets/plane.png"))
    val speed = Random().nextInt(5)+1
    override fun update() {
        positionX -= speed
        positionY += speed+6/4

        if(positionY>Main.HEIGHT+80 || positionX<-80)isDestroyed = true
    }

    override fun render(g: Graphics) {
        g.drawImage(planeFromRightTexture, positionX.toInt(), positionY.toInt(), null)
    }

    override fun collideReact(hitObjects: ArrayList<Enemies>) {
    }

    override fun getBound(): Rectangle = Rectangle(0,0,0,0)

    override fun dispose() {
    }
}