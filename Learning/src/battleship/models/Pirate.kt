package battleship.models

import battleship.GameManager
import java.awt.Graphics
import java.awt.Rectangle
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

/**
 * Created by KIEN on 6/12/2017.
 */
class Pirate(x:Double, y: Double): Enemies(x, y){
    val width:Int = 40
    val height:Int = 80
    val pirateRegion:BufferedImage = ImageIO.read(File("src/battleship/assets/pirate.png"))
    var pirateTexture:BufferedImage = grabImage(pirateRegion,0, width, height)
    val explosionRegion:BufferedImage = ImageIO.read(File("src/battleship/assets/explosion.png"))
    var explosionTexture:BufferedImage?=null
    var health:Int = 2
    var isHit:Boolean = false
    var startAnimate = System.currentTimeMillis()
    var count:Int = 0

    override fun update() {
        if(isHit)
            animate()
        if(health<=0){
            pirateTexture = grabImage(pirateRegion, 1, width, height)
            count++
        }
        if(count>15) {
            isDestroyed = true
            GameManager.point++
        }
        positionY+=2
    }

    override fun render(g: Graphics) {
        g.drawImage(pirateTexture, positionX.toInt(), positionY.toInt(), null)
        if(explosionTexture!=null){
            g.drawImage(
                    explosionTexture,
                    positionX.toInt(),
                    if(health>=1) (positionY+40).toInt() else positionY.toInt(),
                    null
            )
        }
    }

    override fun getBound(): Rectangle =
            if(health>0)Rectangle(positionX.toInt()+5, positionY.toInt()+5, width-5, height-5)
            else Rectangle(0,0,0,0)

    override fun setDestruction() {
        health--
        startAnimate = System.currentTimeMillis()
        isHit = true
    }

    override fun dispose() {
    }

    fun animate(){
        val frame = ((System.currentTimeMillis()-startAnimate)/100).toInt()
        if(frame<5) {
            explosionTexture = grabImage(explosionRegion, frame, 40, 40)
        }else{
            explosionTexture = null
            isHit = false
        }
    }
}