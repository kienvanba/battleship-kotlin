package battleship.models

import java.awt.Graphics
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

/**
 * Created by KIEN on 6/14/2017.
 */
class HeathBar(var positionX:Double, var positionY:Double, val ship:Ship){
    val with:Int = 160
    val height:Int = 60
    val heathBarTexture: BufferedImage = ImageIO.read(File("src/battleship/assets/heath-bar.png"))
    val heathRateTexture: BufferedImage = ImageIO.read(File("src/battleship/assets/heath-rate.png"))
    var actualHeath: BufferedImage = grabImage(ship.health*19)
    var lastHeath = ship.health
    var i=1

    fun update(){
        if(ship.health!=lastHeath) {
            actualHeath = grabImage(lastHeath * 19-i++)
            if(i==19) {
                lastHeath = ship.health
                i = 1
            }
        }
    }

    fun render(g: Graphics){
        g.drawImage(heathBarTexture, positionX.toInt(), positionY.toInt(), with, height, null)
        g.drawImage(actualHeath, positionX.toInt()+59, positionY.toInt()+20, null)
    }

    fun grabImage(heathRate: Int):BufferedImage = heathRateTexture.getSubimage(0,0,if(heathRate<=0)1 else heathRate, 15)
}