package battleship.models

import battleship.GameManager
import battleship.Main
import battleship.scenes.WelcomeScene
import java.awt.Graphics
import java.awt.Point
import java.awt.event.MouseEvent
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

/**
 * Created by KIEN on 6/15/2017.
 */
class IngameDropMenu{
    val root = -200.0
    var positionX:Double = Main.WIDTH-205.0
    var positionY:Double = root
    var destinationY:Double = positionY
    val backgroundTexture:BufferedImage = ImageIO.read(File("src/battleship/assets/ingame-menu-background.png"))
    val btnExitTexture:BufferedImage = ImageIO.read(File("src/battleship/assets/to-main-btn.png"))
    val btnExitTextureHover:BufferedImage = ImageIO.read(File("src/battleship/assets/to-main-btn-hover.png"))
    val width:Int = backgroundTexture.width
    val height:Int = backgroundTexture.height
    var speed = 5
    val btnExit:Button = Button(positionX+145.0, positionY+100, btnExitTexture, btnExitTextureHover)
    init {
        btnExit.speed = speed
    }

    fun update(){
        if(positionY.near(destinationY, speed)<0){
            positionY+=speed
        }else if(positionY.near(destinationY, speed)>0){
            positionY-=speed
        }
        btnExit.positionY = positionY+100
        btnExit.update()
    }

    fun render(g:Graphics){
        g.drawImage(backgroundTexture, positionX.toInt(), positionY.toInt(), width, height, null)
        btnExit.render(g)
    }

    fun onMousePressed(e:MouseEvent){
        val clickPoint = Point(e.x, e.y)
        btnExit.isHovering = btnExit.getBound().contains(clickPoint)
    }

    fun onMouseReleased(e:MouseEvent){
        val releasePoint = Point(e.x, e.y)
        btnExit.isHovering = false
        if(btnExit.getBound().contains(releasePoint))
            GameManager.addScenes(WelcomeScene())
    }

    fun Double.near(number:Double, range:Int):Int{
        if(Math.abs((this-number).toInt())<=range)
            return 0
        return (this-number).toInt()
    }
}