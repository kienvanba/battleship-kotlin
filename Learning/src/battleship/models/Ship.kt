package battleship.models

import battleship.GameManager
import battleship.Main
import java.awt.Graphics
import java.awt.Point
import java.awt.Rectangle
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

/**
 * Created by KIEN on 6/8/2017.
 */
class Ship(x:Double, y:Double): Allies(x,y){
    companion object{
        val SPEED:Double = 3.5
    }
    val width:Int = 40
    val height:Int = 80
    var velocityX:Double = 0.0
    var velocityY:Double = 0.0
    var shipTexture:BufferedImage = ImageIO.read(File("src/battleship/assets/ship.png"))
    var brokenTexture:BufferedImage = ImageIO.read(File("src/battleship/assets/broken-ship.png"))
    var emptyTexture:BufferedImage = ImageIO.read(File("src/battleship/assets/empty.png"))
    var texture = shipTexture
    var isBroken:Boolean = false
    var health:Int = 5
    var justHit:Boolean = false
    var lastTime = System.currentTimeMillis()
    var animationTiming: Double = 0.0

    override fun update() {
        if(justHit){
            animate()
        }else{
            if(texture==emptyTexture && !isBroken)texture=shipTexture
        }
        if(!isBroken){
            if(System.currentTimeMillis()-lastTime>2500 && justHit){
                justHit = false
            }
            positionX += velocityX
            positionY += velocityY
            reposition()
        }else{
            texture = brokenTexture
        }
    }

    override fun render(g: Graphics) {
        g.drawImage(texture, positionX.toInt(), positionY.toInt(), width, height, null)
    }

    override fun dispose() {
        shipTexture.flush()
    }

    override fun collideReact(hitObjects: ArrayList<Enemies>) {
        if(!justHit && !isBroken) {
            hitObjects
                    .filter { getBound().intersects(it.getBound()) }
                    .forEach {
                        it.setDestruction()
                        health--
                        if (health <= 0) {
                            isBroken = true
                        } else {
                            justHit = true
                            lastTime = System.currentTimeMillis()
                        }
                        println("kaboom!")
                    }
        }
    }

    override fun getBound(): Rectangle {
        return Rectangle(positionX.toInt(), positionY.toInt(), width, height)
    }

    fun reposition(){
        if(positionX>Main.WIDTH-width)positionX=Main.WIDTH-width.toDouble()
        if(positionX<0)positionX=0.0
        if(positionY>Main.HEIGHT-height)positionY=Main.HEIGHT-height.toDouble()
        if(positionY<0)positionY=0.0
    }

    override fun toString(): String {
        return "[x: $positionX, y: $positionY]"
    }

    fun animate(){
        animationTiming+=0.1
        if(animationTiming.toInt()%2==0){
            texture = shipTexture
        }else{
            texture = emptyTexture
        }
    }
}