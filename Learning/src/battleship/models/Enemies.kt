package battleship.models

import java.awt.image.BufferedImage

/**
 * Created by KIEN on 6/10/2017.
 */
abstract class Enemies(x: Double, y: Double): IModel{
    var positionX:Double = x
    var positionY:Double = y
    var isDestroyed = false
    var isExploded = false

    abstract fun setDestruction()
    fun grabImage(region: BufferedImage, pos: Int, width: Int, height: Int): BufferedImage =
            region.getSubimage(pos*width, 0, width, height)
}