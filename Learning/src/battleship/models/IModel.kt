package battleship.models

import java.awt.Graphics
import java.awt.Point
import java.awt.Rectangle

/**
 * Created by KIEN on 6/8/2017.
 */
interface IModel {
    fun update()
    fun render(g: Graphics)
    fun getBound(): Rectangle
    fun dispose()
}