package battleship.models

import java.awt.Graphics
import java.awt.Point
import java.awt.Rectangle
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

/**
 * Created by KIEN on 6/9/2017.
 */
class Bullet(x:Double, y:Double): Allies(x,y){
    val width:Int = 20
    val height:Int = 40
    val bulletTexture:BufferedImage = ImageIO.read(File("src/battleship/assets/bullet.png"))
    override fun update() {
        positionY -=10
    }

    override fun render(g: Graphics) {
        g.drawImage(bulletTexture, positionX.toInt(), positionY.toInt(), null)
    }

    override fun getBound(): Rectangle {
        return Rectangle(positionX.toInt(), positionY.toInt(), width, height)
    }

    override fun collideReact(hitObjects: ArrayList<Enemies>) {
        hitObjects
                .filter { it is Pirate && it.getBound().intersects(getBound()) }
                .forEach{
                    it.setDestruction()
                    isDestroyed = true
                }
    }

    override fun dispose() {
        bulletTexture.flush()
    }

}