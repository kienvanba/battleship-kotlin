package battleship.models

import java.awt.Graphics
import java.awt.Point
import java.awt.Rectangle
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

/**
 * Created by KIEN on 6/10/2017.
 */
class Bomb(x:Double, y:Double): Enemies(x,y){
    val width:Int = 60
    val height:Int = 40
    var boomTexture:BufferedImage = ImageIO.read(File("src/battleship/assets/boom.png"))
    var explosionRegion:BufferedImage = ImageIO.read(File("src/battleship/assets/explosion-animation.png"))
    var startAnimate = System.currentTimeMillis()

    override fun update() {
        if(isExploded)
            animate()
        positionY+=2
    }

    override fun render(g: Graphics) {
        g.drawImage(boomTexture, positionX.toInt(), positionY.toInt(), null)
    }

    override fun getBound(): Rectangle {
        return Rectangle(positionX.toInt()+5, positionY.toInt()+5, width-5, height-5)
    }

    override fun setDestruction() {
        isExploded = true
        startAnimate = System.currentTimeMillis()
    }

    override fun dispose() {
    }

    fun animate(){
        val frame = ((System.currentTimeMillis()-startAnimate)/150).toInt()
        if(frame<5) {
            boomTexture = grabImage(explosionRegion, frame, 80, 80)
        }else{
            isDestroyed = true
        }
    }
}