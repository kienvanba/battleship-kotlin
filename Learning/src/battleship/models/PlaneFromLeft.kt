package battleship.models

import java.awt.Graphics
import java.awt.Point
import java.awt.Rectangle
import java.awt.image.BufferedImage
import java.io.File
import java.util.*
import javax.imageio.ImageIO
import kotlin.collections.ArrayList

/**
 * Created by KIEN on 6/18/2017.
 */
class PlaneFromLeft(positionX:Double, positionY:Double):Allies(positionX, positionY){
    val planeFromLeftTexture: BufferedImage = ImageIO.read(File("src/battleship/assets/plane2.png"))
    var speed = Random().nextInt(5)+1

    override fun update() {
        positionX += speed
        positionY += speed+6/4

        if(positionY<=-80 || positionX>480)
            isDestroyed = true
    }

    override fun render(g: Graphics) {
        g.drawImage(planeFromLeftTexture, positionX.toInt(), positionY.toInt(), null)
    }

    override fun getBound(): Rectangle = Rectangle(0,0,0,0)

    override fun dispose() {
    }

    override fun collideReact(hitObjects: ArrayList<Enemies>) {
    }
}