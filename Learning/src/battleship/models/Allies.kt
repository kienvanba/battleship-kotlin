package battleship.models

/**
 * Created by KIEN on 6/10/2017.
 */
abstract class Allies(var positionX: Double, var positionY: Double): IModel {
    var isDestroyed:Boolean = false

    abstract fun collideReact(hitObjects: ArrayList<Enemies>)
}