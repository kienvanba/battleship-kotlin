package battleship.models

import java.awt.Graphics
import java.awt.Rectangle
import java.awt.image.BufferedImage

/**
 * Created by KIEN on 6/13/2017.
 */
class Button(var positionX:Double,
             var positionY:Double,
             val menuTexture: BufferedImage,
             val hoverTexture: BufferedImage
){
    val rootX = positionX
    val rootY = positionY
    var isHovering = false
    var destinationX:Double = positionX
    var destinationY:Double = positionY
    val width:Int = menuTexture.width
    val height:Int = menuTexture.height
    var speed = 5

    fun update(){
        if(positionX.near(destinationX,speed)<0){
            positionX+=speed
        }else if(positionX.near(destinationX,speed)>0){
            positionX-=speed
        }
        if(positionY.near(destinationY,speed)<0){
            positionY+=speed
        }else if(positionY.near(destinationY,speed)>0){
            positionY-=speed
        }
    }

    fun render(g: Graphics){
        g.drawImage(if(isHovering)hoverTexture else menuTexture, positionX.toInt(), positionY.toInt(), width, height, null)
    }

    fun getBound():Rectangle = Rectangle(positionX.toInt(), positionY.toInt(), width, height)

    fun Double.near(number:Double, range:Int):Int{
        if(Math.abs((this-number).toInt())<=range)
            return 0
        return (this-number).toInt()
    }
}