package battleship

import java.awt.Graphics
import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.JPanel

/**
 * Created by KIEN on 6/8/2017.
 */
class MainScene: JPanel(){
    init {
        initData()
        initListener()
    }
    fun initData(){
        setSize(Main.WIDTH, Main.HEIGHT)
        isFocusable = true
    }

    /**
     *  todo: add key and mouse listener
     */
    fun initListener(){
        addMouseListener(object: MouseAdapter(){
            override fun mousePressed(e: MouseEvent?) {
                if(e==null)return
                requestFocusInWindow()
                GameManager.getCurrentScene().onMousePressed(e)
            }

            override fun mouseReleased(e: MouseEvent?) {
                if(e==null)return
                GameManager.getCurrentScene().onMouseReleased(e)
            }
        })

        addKeyListener(object: KeyAdapter(){
            override fun keyPressed(e: KeyEvent?) {
                if(e==null)return
                GameManager.getCurrentScene().onKeyPressed(e)
            }

            override fun keyReleased(e: KeyEvent?) {
                if(e==null)return
                GameManager.getCurrentScene().onKeyReleased(e)
            }
        })
    }

    /**
     * todo: update variables and perform actions
     */
    fun update(){
        GameManager.getCurrentScene().update()
    }

    /**
     *  todo: render images and textures to screen
     */
    override fun paintComponent(g: Graphics?) {
        super.paintComponent(g)
        if(g==null)return
        GameManager.getCurrentScene().render(g)
    }
}