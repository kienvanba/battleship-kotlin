package level2

import java.util.*
import javax.swing.JFrame

/**
 * Created by KIEN on 6/3/2017.
 */

fun Date.nextDate(){
    this.date+=1
}
fun main(args: Array<String>) {
    var mDate:Date = Date()
    println(mDate.date)
    mDate.nextDate()
    println(mDate.date)

    val mFrame:JFrame = JFrame()
    mFrame.setSize(400,500)
    mFrame.isVisible = true
}