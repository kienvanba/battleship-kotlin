package first.product.scenes

/**
 * Created by KIEN on 6/8/2017.
 */
interface Scene {
    fun update()
    fun render()
    fun dispose()
}