package first.product.models

import java.awt.Graphics

/**
 * Created by KIEN on 6/3/2017.
 */
interface IModel {
    fun draw(g: Graphics)
}