package first.product.models

import first.product.GameManager
import java.awt.Graphics
import java.awt.Point
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

/**
 * Created by KIEN on 6/3/2017.
 */
class Ship(pos:Point): IModel {
    var position = pos
    var texture:BufferedImage = ImageIO.read(File("images/ship-2.png"))
    companion object{
        val LEFT:Int = 1
        val RIGHT:Int = 2
        val FORWARD:Int = 3
        val BACKWARD:Int = 4
    }

    override fun draw(g: Graphics) {
        g.drawImage(texture, position.x, position.y, null)
    }
    fun move(direction:Int){
        when(direction){
            LEFT -> position.x--
            RIGHT -> position.x++
            FORWARD -> position.y--
            BACKWARD -> position.y++
        }
    }
    fun Point.isNear(position: Point, range:Int): Boolean{
        return ((Math.abs(position.x-this.x)<=range) && (Math.abs(position.y-this.y)<=range))
    }
}