package first.product

import first.product.scenes.Scene
import java.util.*

/**
 * Created by KIEN on 6/8/2017.
 */
object GameManager {
    var scenes:Stack<Scene> = Stack()

    fun addScenes(scene: Scene){
        if(!scenes.isEmpty()){
            scenes.pop().dispose()
        }
        scenes.push(scene)
    }
}