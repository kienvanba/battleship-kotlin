package first.product

import first.product.models.Ship
import java.awt.Graphics
import java.awt.Point
import java.awt.event.*
import java.awt.event.KeyEvent.VK_UP
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import javax.swing.JFrame
import javax.swing.JPanel

/**
 * Created by KIEN on 6/3/2017.
 */
class MainScreen: JFrame() {
    val WIDTH:Int = 407
    val HEIGHT:Int = 630
    var mContainer:Container = Container()
    init {
        setSize(WIDTH, HEIGHT)
        isVisible = true
        defaultCloseOperation = EXIT_ON_CLOSE
        contentPane = mContainer
        isResizable = false
//        validate()
//        GameThread(mContainer).start()
    }
}

class Container: JPanel() {
    var mShip:Ship?=null
    init {
        initData()
        initListener()
    }
    fun initData(){
        isFocusable = true
        mShip = Ship(Point(200,520))
    }
    fun initListener(){
        addMouseListener(object: MouseAdapter(){
            override fun mousePressed(e: MouseEvent?) {
                if(e==null)return
                requestFocusInWindow()
                println("Pressed at: ["+e.x+","+e.y+"]")
            }
        })

        addKeyListener(object: KeyAdapter(){
            override fun keyPressed(e: KeyEvent?) {
                println("pressed")
                if(e==null)return
                when(e.keyCode){
                    KeyEvent.VK_LEFT -> mShip!!.move(Ship.LEFT)
                    KeyEvent.VK_RIGHT -> mShip!!.move(Ship.RIGHT)
                    KeyEvent.VK_UP -> mShip!!.move(Ship.FORWARD)
                    KeyEvent.VK_DOWN -> mShip!!.move(Ship.BACKWARD)
                }
                if(e.keyCode==KeyEvent.VK_LEFT && e.keyCode==VK_UP){
                    mShip!!.move(Ship.LEFT)
                    mShip!!.move(Ship.FORWARD)
                }
                repaint()
            }
        })
    }

    override fun paintComponent(g: Graphics?) {
        super.paintComponent(g)
        if(g==null)return
        val bg:BufferedImage = ImageIO.read(File("images/game-bg.jpg"))
        g.drawImage(bg, 0, 0, null)
        mShip!!.draw(g)
    }
}
class GameThread(container:Container): Thread(){
    var mContainer = container
    var isRunning:Boolean = false
    override fun run() {
        super.run()
        while(isRunning) {
            mContainer.repaint()
        }
    }
}